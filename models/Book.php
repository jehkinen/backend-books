<?php

namespace app\models;

use app\traits\SaveRelationsTrait;
use Yii;
use yii\behaviors\TimestampBehavior;
use app\behaviors\SaveRelationsBehavior;

/**
 * This is the model class for table "book".
 *
 * @property int $id
 * @property string $title
 * @property string $author
 * @property string $isbn
 * @property int $created_at
 * @property int $updated_at
 *
 * @property BookGenreAssn[] $bookGenreAssns
 * @property Genre[] $genres
 *
 */
class Book extends \yii\db\ActiveRecord
{
    use SaveRelationsTrait;

    public function behaviors()
    {
        return [
        TimestampBehavior::class,
        'saveRelation' => [
            'class' => SaveRelationsBehavior::class,
            'relations' => ['genres'],
        ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'book';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'author', 'isbn'], 'required'],
            ['isbn', 'unique'],
            [['title', 'author', 'isbn'], 'string', 'max' => 255],
            [['created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'author' => Yii::t('app', 'Author'),
            'isbn' => Yii::t('app', 'isbn'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookGenreAssns()
    {
        return $this->hasMany(BookGenreAssn::class, ['book_id' => 'id']);
    }

    public function getGenres()
    {
        return $this->hasMany(Genre::class, ['id' => 'genre_id'])->via('bookGenreAssns');
    }

    public function beforeDelete()
    {
        BookGenreAssn::deleteAll(['book_id' => $this->id]);
        return parent::beforeDelete();
    }
}
