<?php

namespace app\modules\api\controllers;

use app\modules\api\models\Book;
use yii\data\ActiveDataProvider;

class BookController extends BaseController
{
    public function actions() {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = function () {
            return new ActiveDataProvider([
                'query' => Book::find(),
                'sort'  =>  [
                    'defaultOrder'  =>  [
                        'id'    =>  SORT_DESC
                    ]
                ]
            ]);
        };
        return $actions;
    }

    public $modelClass = Book::class;
}
