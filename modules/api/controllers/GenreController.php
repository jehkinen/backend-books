<?php

namespace app\modules\api\controllers;

use app\modules\api\models\Genre;
use yii\data\ActiveDataProvider;

class GenreController extends BaseController
{
    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = function () {
            return new ActiveDataProvider([
                'query' => Genre::find(),
                'sort' => [
                    'defaultOrder' => [
                        'id' => SORT_DESC
                    ]
                ]
            ]);
        };
        return $actions;
    }

    public $modelClass = Genre::class;
}
