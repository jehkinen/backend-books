<?php
namespace app\modules\api\controllers;
use Yii;
use app\models\LoginForm;

class SiteController extends BaseController
{
    public function actionIndex()
    {
        return 'book-api v.1.0';
    }

    public function actionLogin()
    {
        $model = new LoginForm();

        $model->load(Yii::$app->request->bodyParams, '');
        if ($token = $model->auth()) {
            return $token;
        } else {
            return $model;
        }
    }
    protected function verbs()
    {
        return [
            'login' => ['post', 'options'],
        ];
    }
}
