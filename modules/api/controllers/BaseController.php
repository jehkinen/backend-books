<?php

namespace app\modules\api\controllers;

use app\behaviors\HttpBearerAuth;
use app\modules\api\models\Book;
use yii\filters\Cors;
use yii\rest\ActiveController;


class BaseController extends ActiveController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator']['only'] = ['create', 'update', 'delete'];
        $behaviors['authenticator']['except'] = ['login', 'options'];

        $behaviors['authenticator']['authMethods'] = [
            HttpBearerAuth::class,
        ];

        $behaviors['corsFilter'] = [
            'class' => Cors::class,
        ];
        return $behaviors;
    }

    public $modelClass = Book::class;
}
