<?php

namespace app\modules\api\models;


class Genre extends \app\models\Genre {

    public function fields()
    {
        return [
            'id', 'title'
        ];
    }

}
