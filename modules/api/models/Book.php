<?php

namespace app\modules\api\models;


class Book extends \app\models\Book {

    public function fields()
    {
        return [
            'id', 'title', 'author', 'isbn'
        ];
    }

    public function extraFields()
    {
        return ['genres'];
    }
}
