/*
SQLyog Professional v10.42 
MySQL - 8.0.12 : Database - testjob
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `book` */

DROP TABLE IF EXISTS `book`;

CREATE TABLE `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `isbn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `genre_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

/*Data for the table `book` */

insert  into `book`(`id`,`title`,`author`,`isbn`,`created_at`,`updated_at`,`genre_id`) values (1,'ULYSSES','James Joyce','0939370115',1540636028,1540688499,NULL),(2,'THE GREAT GATSBY','F. Scott Fitzgerald.','1435144589',1540636028,1540688508,NULL),(7,'THE SOUND AND THE FURY','William Faulkner.','1119212049',1540636028,1540687513,NULL),(52,'Don Quixote','Miguel de Cervantes','1423641752',1540671244,1540688713,NULL),(64,' Hamlet','William Shakespeare','1435154460',1540687580,1540687580,NULL);

/*Table structure for table `book_genre_assn` */

DROP TABLE IF EXISTS `book_genre_assn`;

CREATE TABLE `book_genre_assn` (
  `book_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL,
  KEY `fk_book_genre_assn___book_id` (`book_id`),
  KEY `fk_book_genre_assn___genre_id` (`genre_id`),
  CONSTRAINT `fk_book_genre_assn___book_id` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`),
  CONSTRAINT `fk_book_genre_assn___genre_id` FOREIGN KEY (`genre_id`) REFERENCES `genre` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `book_genre_assn` */

insert  into `book_genre_assn`(`book_id`,`genre_id`) values (52,3),(52,6),(64,4),(64,5),(64,2),(64,10),(64,9),(64,8);

/*Table structure for table `genre` */

DROP TABLE IF EXISTS `genre`;

CREATE TABLE `genre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/*Data for the table `genre` */

insert  into `genre`(`id`,`title`,`created_at`,`updated_at`) values (1,'Narrative Nonfiction',1540636028,1540636028),(2,'Horror fiction',1540636028,1540636028),(3,'Biography ',1540636028,1540636028),(4,'Drama ',1540636028,1540636028),(5,'Poetry ',1540636028,1540636028),(6,'Fantasy ',1540636028,1540636028),(7,'Humor ',1540636028,1540636028),(8,'Science Fiction',1540636028,1540636028),(9,'Historical Fiction ',1540636028,1540636028),(10,'Mystery ',1540636028,1540636028),(11,'Legend ',1540636028,1540636028),(12,'Fiction in Verse ',1540636028,1540636028);

/*Table structure for table `token` */

DROP TABLE IF EXISTS `token`;

CREATE TABLE `token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `expired_at` int(11) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

/*Data for the table `token` */

insert  into `token`(`id`,`user_id`,`expired_at`,`token`,`created_at`,`updated_at`) values (12,1,1540784343,'rqO3Ya9VUFlGpuBMNLKF5RkR6PxSe8uw',1540697943,1540697943),(13,1,1540785687,'Nccnu8sH-FACjj3puSBUl4b_lZrSEd5w',1540699287,1540699287),(14,1,1540785707,'R5lsB--YPywzWo0g0OCpGDWmnFY_7mT3',1540699307,1540699307),(15,1,1540787069,'yz4DpShgCyF3hAiSUMTQL-nVBUnfaMN2',1540700669,1540700669),(16,1,1540787150,'hQR7kQrE6iueWMCJaTZll6aglGYzD5v6',1540700750,1540700750);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_user_username` (`username`),
  UNIQUE KEY `idx_user_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`email`,`password_hash`,`auth_key`,`updated_at`,`created_at`) values (1,'admin','andy@example.com','$2y$10$l37y93twwnZiUMf.dgBwqegkDWtra22V9obnmzfo2BF.cNQrCWOv.','KX8jQUyAyykX9sfA-1_w6x0wdM3qKemq',1478273516,1478273516),(6,'maggy','maggy@example.com','$2y$10$l37y93twwnZiUMf.dgBwqegkDWtra22V9obnmzfo2BF.cNQrCWOv.','ZuDE_mlozxGhbPzs9hkz0W177_Lge6w2',1534841762,1534841762);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
